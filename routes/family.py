# Stdlib
from io import BytesIO
import json as _json
import os

# External Libraries
from flask import abort, request, send_file
from flask.json import jsonify
import graphviz

# CJ2 Internals
from framework.cj2 import CJ2
from framework.response_wrappers import json, auth_has_ratelimit
from framework.route import route
from framework.routecog import RouteCog


class Family(RouteCog):
    def __init__(self, core):
        super().__init__(core)
        with open("resources/json/mythical_info.json") as file:
            self.mythical = _json.load(file)

    def s(self, name: str) -> str:
        """
        Sanitize function to strip whitespace in minimal char count
        """
        return name.replace(" ", "")

    def person_to_label(self, person: dict) -> str:
        """
        Create DOT labels from a dict
        """
        # Select color based on gender; male -> azure2, female -> bisque, other/unknown -> white
        color = ("azure2" if person["gender"] == "male"
                 else ("bisque" if person["gender"] == "female"
                       else "white"))
        return f'{self.s(person["name"])}[label="{person["name"]}",style=filled,fillcolor={color}];'

    def get_relatives(self, name: str) -> list:
        """
        Returns a list of dicts of all direct relatives
        """
        person = self.mythical[name]
        people = []
        for relative_name in person["parents"]:
            if relative_name in self.mythical:
                people.append(self.mythical[relative_name])
            else:
                people.append({"name": relative_name, "gender": "unknown"})

        people.append(person)

        if person["spouse"]:
            if person["spouse"] in self.mythical:
                people.append(self.mythical[person["spouse"]])
            else:
                people.append({"name": person["spouse"],
                               "gender": "female" if person["gender"] == "male" else "male"})
            for relative_name in person["children"]:
                if relative_name in self.mythical:
                    people.append(self.mythical[relative_name])
                else:
                    people.append({"name": relative_name, "gender": "unknown"})

        for relative_name in person["siblings"]:
            if relative_name in self.mythical:
                people.append(self.mythical[relative_name])
            else:
                people.append({"name": relative_name, "gender": "unknown"})

        return people

    def gen_graph(self, name: str) -> BytesIO:
        """
        Generate a DOT graph
        """
        person = self.mythical[name]
        if not person["parents"]:
            abort(404)

        # Credits to adrienverge for the format
        # https://github.com/adrienverge/familytreemaker/blob/master/familytreemaker.py

        # NOTES:
        # hX[_Y] is used as empty point to make 90 degree angles

        # Get all the names of the relatives and put them all on a new line in the correct format
        people = "\n".join(
            self.person_to_label(_person) for _person in self.get_relatives(name)
        )

        # Sadly we're gonna have to use one big string here
        rank_str = ""

        # Set up the parent rank.
        if len(person["parents"]) == 2:
            rank_str += "{ rank=same;"
            rank_str += f"{self.s(person['parents'][0])} -> h0 -> {self.s(person['parents'][1])};"
            rank_str += 'h0[shape=circle,label="",height=0.01,width=0.01];}'

        else:
            rank_str += f"{self.s(person['parents'][0])} -> h0;"
            rank_str += 'h0[shape=circle,label="",height=0.01,width=0.01];'

        pers_rank = 0

        # Set up sibling rank
        rank_str += "{ rank=same; "

        # Here we use three parts because it'd be impossible to do otherwise.
        # p1: Make dots above all people's names
        # p2: Style those dots to be invisible
        # p3: Link all dots to people
        p1 = ""
        p2 = ""
        p3 = ""

        # If even number of (siblings + person), add a point in the center to make this symmetrical
        if len(person['siblings']) % 2:
            insert_pos = (False, len(person['siblings']) / 2 + 2)
        else:
            insert_pos = (True, len(person['siblings']) // 2 + 2)

        if not person["siblings"]:
            p1 += "h0"

        # Process all siblings
        for i, sib in enumerate(person['siblings']):
            if person["spouse"] == sib:  # Because incest
                continue

            if i == insert_pos[1] and person["siblings"]:
                p1 += f"-> h1_{pers_rank}"
                p2 += f'h1_{pers_rank}[shape=circle,label="",height=0.01,width=0.01];'
                p3 += f"h0 -> h1_{pers_rank};"
                pers_rank += 1
            if p1 == "":
                p1 += f"h1_{pers_rank}"

            else:
                p1 += f"-> h1_{pers_rank}"

            p2 += f'h1_{pers_rank}[shape=circle,label="",height=0.01,width=0.01];'
            p3 += f"h1_{pers_rank} -> {self.s(sib)};"

            pers_rank += 1

        # Add our person
        p1 += f"-> h1_{pers_rank}"
        p2 += f'h1_{pers_rank}[shape=circle,label="",height=0.01,width=0.01];'
        p3 += f"h1_{pers_rank} -> {self.s(name)};"

        pers_rank += 1

        if person["spouse"] in person["siblings"]:
            p1 += f"-> h1_{pers_rank}"
            p2 += f'h1_{pers_rank}[shape=circle,label="",height=0.01,width=0.01];'
            p3 += f"h1_{pers_rank} -> {self.s(person['spouse'])};"

        pers_rank = 0

        # Join it all together
        rank_str += p1 + ";" + p2 + "};" + p3

        # If they have a spouse, add the spouse and all their children.
        # This is impossible to do with multiple spouses, which is why we limited the data set
        if person["spouse"]:
            # Person and spouse should be on the same height
            rank_str += "{ rank=same; "
            rank_str += f"{self.s(name)} -> h2 -> {self.s(person['spouse'])};"
            rank_str += 'h2[shape=circle,label="",height=0.01,width=0.01];}'

            # Same here as with siblings
            p1 = ""
            p2 = ""
            p3 = ""

            if person['children']:
                rank_str += "{ rank=same; "

            # Here the values are switched due to no longer accounting for the person themselves)
            if len(person['children']) % 2:
                insert_pos = (True, len(person['children']) // 2 + 1)
            else:
                insert_pos = (False, len(person['children']) / 2 + 1)

            # Process all children
            for i, sib in enumerate(person['children']):
                if i == insert_pos[1] and person["children"]:
                    p1 += f"-> h3_{pers_rank}"
                    p2 += f'h3_{pers_rank}[shape=circle,label="",height=0.01,width=0.01];'
                    p3 += f"h2 -> h3_{pers_rank};"
                    pers_rank += 1
                if p1 == "":
                    p1 += f"h3_{pers_rank}"

                else:
                    p1 += f"-> h3_{pers_rank}"

                p2 += f'h3_{pers_rank}[shape=circle,label="",height=0.01,width=0.01];'
                p3 += f"h3_{pers_rank} -> {self.s(sib)};"

                pers_rank += 1

            rank_str += p1 + ";" + p2 + "};" + p3

        # Put it all together with an f-string
        text = f"""
        digraph {{
            node [shape=box];
            edge [dir=none];

            {people}

            {rank_str}
        }}
        """

        # Render and save as PNG
        with open("a.gv", "w") as f:
            f.write(text)

        engine = request.args.get("engine", "dot")

        if engine not in ("dot", "neato", "twopi", "sfdp", "fdp", "circo"):
            engine = "dot"

        path = graphviz.render(engine, "png", "a.gv")
        with open(path, "rb") as f:
            data = f.read()

        os.remove(path)
        os.remove("a.gv")
        return BytesIO(data)

    @route("/api/family/<string:name>")
    @auth_has_ratelimit()
    def family(self, name):
        if name not in self.mythical:
            abort(404, f"{name} is not in our database.")

        buffer = self.gen_graph(name)

        return send_file(buffer, mimetype="image/png")

    @route("/api/family")
    @json
    @auth_has_ratelimit()
    def family_valid(self):
        return jsonify([person for person, data in self.mythical.items() if data["parents"]])


def setup(core: CJ2):
    Family(core).register()
