# Stdlib
import json as _json

# External Libraries
from flask import abort
from flask.json import jsonify

# CJ2 Internals
from framework.cj2 import CJ2
from framework.response_wrappers import json, auth_has_ratelimit
from framework.route import route
from framework.routecog import RouteCog


class Basic(RouteCog):
    def __init__(self, core):
        super().__init__(core)
        with open("resources/json/mythical_info.json") as file:
            self.mythical_data = _json.load(file)

    @route("/api/person")
    @json
    @auth_has_ratelimit()
    def people(self):
        return jsonify(list(self.mythical_data.keys()))

    @route("/api/person/<string:name>")
    @json
    @auth_has_ratelimit()
    def person_by_name(self, name: str):
        if name in self.mythical_data:
            return jsonify({"information": self.mythical_data[name]})
        abort(404, f"{name} is not in our database.")

    @route("/api/person/<string:name>/<string:field>")
    @json
    @auth_has_ratelimit()
    def person_description(self, name: str, field: str):
        if name in self.mythical_data and field in self.mythical_data[name]:
            person = self.mythical_data[name]
            return jsonify({
                "information": {
                    "name": person["name"],
                    "description": person[field]
                }
            })
        abort(404, f"{name} is not in our database or {field} isn't a field.")


def setup(core: CJ2):
    Basic(core).register()
