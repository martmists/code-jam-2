# Stdlib
import os

# External Libraries
from flask import Response, abort
from flask.json import jsonify
from markovify import Text

# CJ2 Internals
from framework.cj2 import CJ2
from framework.response_wrappers import json, auth_has_ratelimit
from framework.route import route
from framework.routecog import RouteCog


class Markov(RouteCog):
    def __init__(self, core: CJ2):
        super().__init__(core)

        self.markovs = {}
        for filename in os.listdir("resources/plaintext"):
            with open(f"resources/plaintext/{filename}") as f:
                self.markovs[filename.replace(".txt", "")] = Text(f.read())

    @route("/api/markov")
    @json
    @auth_has_ratelimit()
    def markov_list(self):
        return jsonify(list(self.markovs.keys()))

    @route("/api/markov/<string:text>")
    @json
    @auth_has_ratelimit()
    def markov(self, text: str):
        if text in self.markovs:
            return Response(self.markovs[text].make_sentence())
        abort(404, f"Text {text} not found in our database.")


def setup(core: CJ2):
    Markov(core).register()
