# pylint: skip-file
# flake8: noqa

# External Libraries
import pytest

# CJ2 Internals
from framework.objects import cj2_instance


@pytest.fixture
def app():
    if not cj2_instance.debug:
        cj2_instance.debug = True
        cj2_instance.gather("routes")
    return cj2_instance


def test_trivia(client):
    resp = client.get("/api/trivia")
    assert 200 <= resp._status_code < 300
    assert all(
        val in resp.json["result"] for val in ("question", "answer", "comment")
    )


def test_markov(client):
    resp = client.get("/api/markov")
    assert 200 <= resp._status_code < 300
    assert sorted(resp.json["result"]) == sorted([
        "nature_of_things", "odyssey", "iliad", "children_of_odin"
    ])


def test_markovs(client):
    for f in ["nature_of_things", "odyssey", "iliad", "children_of_odin"]:
        resp = client.get(f"/api/markov/{f}")
        assert 200 <= resp._status_code < 300
