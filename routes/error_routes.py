# External Libraries
from flask import Response, abort

# CJ2 Internals
from framework.cj2 import CJ2
from framework.objects import limiter
from framework.response_wrappers import auth_only
from framework.route import route
from framework.routecog import RouteCog


class ErrorRoutes(RouteCog):
    def __init__(self, core: CJ2):
        super().__init__(core)
        self.logger.debug("Loading Error Routes for debugging purposes")

    @route("/error/400")
    @limiter.exempt
    def do_400(self):
        return abort(400)

    @route("/error/401")
    @limiter.exempt
    def do_401(self):
        return abort(401)

    @route("/error/403")
    @auth_only
    @limiter.exempt
    def do_403(self):
        return Response(
            "This is only shown when correct API-KEY header is provided"
        )

    @route("/error/405", methods=["POST"])
    @limiter.exempt
    def do_405(self):
        return Response("This is only shown on POST requests")

    @route("/error/429")
    @limiter.limit("1 per hour")
    def do_429(self):
        return Response("This is shown once per hour")


def setup(core: CJ2):
    if core.debug:
        ErrorRoutes(core).register()
