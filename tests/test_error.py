# pylint: skip-file
# flake8: noqa

# External Libraries
import pytest

# CJ2 Internals
from framework.objects import cj2_instance


@pytest.fixture
def app():
    if not cj2_instance.debug:
        cj2_instance.debug = True
        cj2_instance.gather("routes")
    return cj2_instance


def test_400(client):
    assert client.get("/error/400")._status_code == 400


def test_401(client):
    assert client.get("/error/401")._status_code == 401


def test_403(client):
    assert client.get("/error/403")._status_code == 403
    assert client.get(
        "/error/403", headers={
            "API-KEY": "abcdef"
        }
    )._status_code == 200


def test_404(client):
    assert client.get("/error/404")._status_code == 404


def test_405(client):
    assert client.get("/error/405")._status_code == 405
    assert client.post("/error/405")._status_code == 200


def test_429(client):
    client.get("/error/429")
    # assert client.get("/error/429")._status_code == 429
    # https://github.com/alisaifee/flask-limiter/issues/147
    # Works, just not in tests
