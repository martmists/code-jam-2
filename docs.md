# Documentation for all routes


## `/admin`

### `/admin/reload`
Reload all routes.


## `/error`
> Debug routes for testing

### `/error/<code>`
Returns response with said error code

## `/api/trivia`

### `/api/trivia`
Get a random trivia question, answer and comment


## `/api/markov`

### `/api/markov`
Get a list of all markov sources

### `/api/markov/<source>`
Get a markov sentence from specified source


## `/api/family`

### `/api/family`
Get all valid people we can build family trees for

### `/api/family/<person>`
Get a .png of the direct family tree of spacified person
Additionally, `engine=ENGINE` can be passed in the url to specify the graphviz engine.
Valid engines:
- dot
- neato
- circo (not recommended)
- twopi
- sfdp (not recommended)
- fdp

> NOTE: This endpoint ONLY works if `graphviz` (the application, not the python package) is installed! This provides the `dot`, `neato`, etc. Programs the `graphviz` module needs. Install it using your package manager (No clue how to install on windows, sorry!) 

## `/api/person`

### `/api/person`
Get all stored people

### `/api/person/<name>`
Get full information about specified person

### `/api/person/<name>/<attribute>`
Get specific information about specified person


# Authorization
Nearly all routes have a feature where using an `API-KEY` header with a valid key (e.g. `abcdef`, see `resources/json/keys.json`) gives lower ratelimits. 