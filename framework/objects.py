# Stdlib
import logging

# External Libraries
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

# CJ2 Internals
from framework.authentication import Authenticator
from framework.cj2 import CJ2

__all__ = ("cj2_instance", "limiter", "logger", "auth_service")

cj2_instance = CJ2()

limiter = Limiter(
    cj2_instance,
    key_func=get_remote_address,
    default_limits=["1 per 2 seconds", "20 per minute", "1000 per hour"]
)

logger = logging.getLogger("CJ2")
logger.setLevel(logging.INFO)

auth_service = Authenticator("resources/json/keys.json")
auth_service.reload_tokens()
