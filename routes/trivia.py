# Stdlib
import json as _json
import random

# External Libraries
from flask.json import jsonify

# CJ2 Internals
from framework.cj2 import CJ2
from framework.response_wrappers import json, auth_has_ratelimit
from framework.route import route
from framework.routecog import RouteCog


class Trivia(RouteCog):
    def __init__(self, core):
        super().__init__(core)
        with open("resources/json/trivia.json") as file:
            self._trivia = _json.load(file)

    @route("/api/trivia")
    @json
    @auth_has_ratelimit("1 per second", "40 per minute", "2000 per hour")
    def trivia(self):
        return jsonify(random.choice(self._trivia))


def setup(core: CJ2):
    Trivia(core).register()
