# Stdlib
import sys

# CJ2 Internals
from framework.objects import cj2_instance

cj2_instance.debug = (len(sys.argv) > 1 and
                      sys.argv[1] == "--debug")
cj2_instance.gather("routes")
cj2_instance.run()
