# Stdlib
import functools

# CJ2 Internals
from framework.cj2 import CJ2

__all__ = ("route", "Route")


def route(path, **kwargs):
    """
    Wraps a function to turn it into a `Route`.
    """
    def decorator(func):
        return Route(func, path, **kwargs)

    return decorator


class Route:
    """
    Route class wrapper to register them on the application
    """
    def __init__(self, func, path: str, **kwargs):
        self.func = func
        self.path = path
        self.kwgs = kwargs
        self.parent = None

    def register(self, core: CJ2):
        _route = core.route(self.path, **self.kwgs)
        func = functools.wraps(self.func)(
            functools.partial(self.func, self.parent)
        )
        _route(func)

    def set_parent(self, parent):
        self.parent = parent
